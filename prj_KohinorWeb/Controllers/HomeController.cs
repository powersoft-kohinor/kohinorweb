﻿using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using prj_KohinorWeb.Models;
using prj_KohinorWeb.Services;
using System.Diagnostics;

namespace prj_KohinorWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly EmailService _emailService;

        public HomeController(ILogger<HomeController> logger, EmailService emailService)
        {
            _logger = logger;
            _emailService = emailService;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Terms()
        {
            return View();
        }
        public IActionResult LITE()
        {
            return View();
        }
        public IActionResult PLUS()
        {
            return View();
        }
        public IActionResult BASIC()
        {
            return View();
        }
        public IActionResult PRO()
        {
            return View();
        }
        public IActionResult LEGACY()
        {
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> SubmitForm([FromBody] ContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = "Por favor, completa todos los campos correctamente." });
            }

            try
            {
                
                var (isSuccess, message) = await _emailService.SendEmail(model);

                return Json(new { success = isSuccess, message });
            }
            catch (Exception ex)
            {
                
                return Json(new { success = false, message = "Ocurrió un error al enviar el formulario. Inténtalo de nuevo más tarde." });
            }
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult ChangeLanguage(string culture)
        {
            Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions() { Expires = DateTimeOffset.UtcNow.AddYears(1) });

            return Redirect(Request.Headers["Referer"].ToString());
        }
    }
}