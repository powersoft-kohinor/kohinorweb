﻿using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using prj_KohinorWeb.Models;
using System.Diagnostics;

namespace prj_KohinorWeb.Controllers
{
    public class AboutUsController : Controller
    {
        private readonly ILogger<AboutUsController> _logger;

        public AboutUsController(ILogger<AboutUsController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

    }
}