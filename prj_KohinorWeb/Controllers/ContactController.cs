﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using prj_KohinorWeb.Models;
using prj_KohinorWeb.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace prj_KohinorWeb.Controllers
{
    public class ContactController : Controller
    {
        private readonly ILogger<ContactController> _logger;
        private readonly HttpClient _httpClient;
        private readonly EmailService _emailService;
        private readonly string _password = "ATATT3xFfGF0k9EZznDfnoKVVhwQ3FszS7J2Kp12iAshyvXg3LgnnC8aeSwgHcA-maS41lIboJY5ErWdQ2KLx0zJOAzQXoOjANSZYzRvvaioAbnEWy1vapq9XDJwdOE6_cLyxyRIm7dMF-ZGKagqom1dCfULxsU3lo3SYfkKRG41frxj6VWa_EQ=CD792A20"; // Replace with your actual API token
        private readonly string _username = "kevin.diaz@kohinorec.com";
      

        public ContactController(ILogger<ContactController> logger, HttpClient httpClient, EmailService emailService)
        {
            _logger = logger;
            _httpClient = httpClient;
            _emailService = emailService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Support()
        {
            return View();
        }
        public IActionResult OrderSent()
        {
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> SubmitContactForm([FromBody] ContactFormModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { success = false, message = "Por favor, completa todos los campos correctamente." });
            }
            try
            {

                var (isSuccess, message) = await _emailService.SendContactEmail(model);

                return Json(new { success = isSuccess, message });
            }
            catch (Exception ex)
            {

                return Json(new { success = false, message = "Ocurrió un error al enviar el formulario. Inténtalo de nuevo más tarde." });
            }
        }
        [HttpPost]
        public async Task<IActionResult> SubmitForm(WorkOrderViewModel formData)
        {
            // Construct the description parameter
            var description = new StringBuilder();
            description.AppendLine($"Nombre: {formData.Name}");
            description.AppendLine($"Email: {formData.Email}");
            description.AppendLine($"Telefono: {formData.PhoneNumber}");
            description.AppendLine($"Requerimiento: {formData.Subject}");
            description.AppendLine($"Empresa: {formData.Company}");
            description.AppendLine($"Instrucciones: {formData.Description}");

            // Create the payload
            var issuePayload = new IssuePayload
            {
                fields = new Fields
                {
                    project = new Project { key = "KDBSWEB" },
                    summary = formData.Subject,
                    description = description.ToString(),
                    issuetype = new IssueType { name = "Task" }
                }
            };

            // Convert the form data to JSON
            var jsonData = JsonConvert.SerializeObject(issuePayload);
            var content = new StringContent(jsonData, Encoding.UTF8, "application/json");

            // Encode the username and password for Basic Authentication
            var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_username}:{_password}"));
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);

            // Send the POST request to the API
            var response = await _httpClient.PostAsync("https://kohinor-team.atlassian.net/rest/api/2/issue", content);

            if (response.IsSuccessStatusCode)
            {
                // Set success message and return the view with the message
                ViewData["Message"] = "Your message has been sent successfully!";
            }
            else
            {
                // Set error message and return the view with the message
                ViewData["Message"] = "An error occurred while sending your message. Please try again.";
            }

            // Return the same view with the message
            return View("OrderSent"); // Make sure the view name matches your actual view name
        }
    }
}
