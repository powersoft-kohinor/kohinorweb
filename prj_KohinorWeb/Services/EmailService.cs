﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using prj_KohinorWeb.Models;

namespace prj_KohinorWeb.Services
{
    public class EmailService
    {
        private readonly AuthService _authService;

        public EmailService(AuthService authService)
        {
            _authService = authService;
        }
        public async Task<(bool IsSuccess, string Message)> SendContactEmail(ContactFormModel model)
        {
            try
            {
                string accessToken = await _authService.GetAccessTokenAsync();
                string sendMailEndpoint = "https://graph.microsoft.com/v1.0/users/wladimir.diaz@kohinorec.com/sendMail";

                var message = new Dictionary<string, object>()
                {
                    {"message", new Dictionary<string, object>()
                        {
                            {"subject", "Nueva Consulta de Soporte"},
                            {"body", new Dictionary<string, object>()
                                {
                                    {"contentType", "HTML"},
                                    {"content", $@"
                                        <p><strong>Nombre Completo:</strong> {model.FullName}</p>
                                        <p><strong>Teléfono:</strong> {model.Phone}</p>
                                        <p><strong>Empresa:</strong> {model.Company}</p>
                                        <p><strong>Email:</strong> {model.Email}</p>
                                        <p><strong>Asunto:</strong> {model.Subject}</p>
                                        <p><strong>Descripción:</strong> {model.Description}</p>"}
                                }
                            },
                            {"toRecipients", new object[]
                                {
                                    new Dictionary<string, object>()
                                    {
                                        {"emailAddress", new Dictionary<string, object>()
                                            {
                                                {"address", "wladimir.diaz@kohinorec.com"}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {"saveToSentItems", "true"}
                };

                var jsonMessage = JsonSerializer.Serialize(message);
                var content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

                using (HttpClient client = new HttpClient())
                {
                    var request = new HttpRequestMessage(HttpMethod.Post, sendMailEndpoint);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    request.Content = content;

                    HttpResponseMessage sendMailResponse = await client.SendAsync(request);
                    string responseContent = await sendMailResponse.Content.ReadAsStringAsync();

                    if (sendMailResponse.IsSuccessStatusCode)
                    {
                        return (true, "El formulario de soporte se envió correctamente.");
                    }
                    else
                    {
                        return (false, $"Error al enviar el formulario de soporte: {responseContent}");
                    }
                }
            }
            catch (Exception ex)
            {
                return (false, $"Error inesperado al enviar el formulario de soporte: {ex.Message}");
            }
        }

        public async Task<(bool IsSuccess, string Message)> SendEmail(ContactFormModel model)
        {
            try
            {
                string accessToken = await _authService.GetAccessTokenAsync();
                string sendMailEndpoint = "https://graph.microsoft.com/v1.0/users/wladimir.diaz@kohinorec.com/sendMail";

                var message = new Dictionary<string, object>()
                {
                    {"message", new Dictionary<string, object>()
                        {
                            {"subject", "Nueva Consulta del Formulario"},
                            {"body", new Dictionary<string, object>()
                                {
                                    {"contentType", "HTML"},
                                    {"content", $@"
                                        <p><strong>Nombre Completo:</strong> {model.FullName}</p>
                                        <p><strong>Teléfono:</strong> {model.Phone}</p>
                                        <p><strong>Empresa:</strong> {model.Company}</p>
                                        <p><strong>Email:</strong> {model.Email}</p>"}
                                }
                            },
                            {"toRecipients", new object[]
                                {
                                    new Dictionary<string, object>()
                                    {
                                        {"emailAddress", new Dictionary<string, object>()
                                            {
                                                {"address", "wladimir.diaz@kohinorec.com"}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {"saveToSentItems", "true"}
                };

                var jsonMessage = JsonSerializer.Serialize(message);
                var content = new StringContent(jsonMessage, Encoding.UTF8, "application/json");

                using (HttpClient client = new HttpClient())
                {
                    var request = new HttpRequestMessage(HttpMethod.Post, sendMailEndpoint);
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    request.Content = content;

                    HttpResponseMessage sendMailResponse = await client.SendAsync(request);
                    string responseContent = await sendMailResponse.Content.ReadAsStringAsync();

                    if (sendMailResponse.IsSuccessStatusCode)
                    {
                        return (true, "El formulario se envió correctamente.");
                    }
                    else
                    {
                        return (false, $"Error al enviar el formulario: {responseContent}");
                    }
                }
            }
            catch (Exception ex)
            {
                return (false, $"Error inesperado: {ex.Message}");
            }
        }
    }
}
