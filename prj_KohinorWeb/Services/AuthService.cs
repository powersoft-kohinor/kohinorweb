﻿using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace prj_KohinorWeb.Services
{
    public class AuthService
    {
        private readonly IConfiguration _configuration;

        public AuthService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<string> GetAccessTokenAsync()
        {
            // Obtener credenciales desde appsettings.json
            var emailSettings = _configuration.GetSection("EmailSettings");
            string clientId = emailSettings["ClientId"];
            string clientSecret = emailSettings["ClientSecret"];
            string tenantId = emailSettings["TenantId"];

            string tokenEndpoint = $"https://login.microsoftonline.com/{tenantId}/oauth2/v2.0/token";

            using (HttpClient client = new HttpClient(new HttpClientHandler
            {
                MaxResponseHeadersLength = int.MaxValue // Configura encabezados sin límite
            }))
            {
                // Configuración del cuerpo de la solicitud
                var parameters = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("client_secret", clientSecret),
                    new KeyValuePair<string, string>("scope", "https://graph.microsoft.com/.default"),
                };

                var requestContent = new FormUrlEncodedContent(parameters);

                // Establece manualmente el encabezado Content-Type
                var request = new HttpRequestMessage(HttpMethod.Post, tokenEndpoint)
                {
                    Content = requestContent
                };

                request.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.SendAsync(request);

                // Verifica si la solicitud fue exitosa
                if (!response.IsSuccessStatusCode)
                {
                    string errorContent = await response.Content.ReadAsStringAsync();
                    throw new Exception($"Error al obtener el token. Respuesta: {errorContent}");
                }

                // Procesa la respuesta y valida el token
                string responseContent = await response.Content.ReadAsStringAsync();

                try
                {
                    // Deserialización manual para evitar truncamiento
                    var tokenResponse = JObject.Parse(responseContent);
                    var accessToken = tokenResponse["access_token"]?.ToString();

                    // Validar longitud del token
                    if (string.IsNullOrWhiteSpace(accessToken) || accessToken.Length < 800)
                    {
                        throw new Exception("El token parece estar truncado.");
                    }

                    Console.WriteLine($"Access Token obtenido exitosamente: {accessToken}");
                    Console.WriteLine($"Longitud del token: {accessToken.Length}");
                    return accessToken;
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error al procesar la respuesta del token: {ex.Message}");
                }
            }
        }
    }
}
