﻿namespace prj_KohinorWeb.Models
{
    public class ContactFormModel
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string? Subject { get; set; }
        public string? Description { get; set; }
    }
}
