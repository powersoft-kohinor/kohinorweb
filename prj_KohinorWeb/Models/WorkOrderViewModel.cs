﻿namespace prj_KohinorWeb.Models
{
    public class WorkOrderViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Subject { get; set; }
        public string Company { get; set; }
        public string Description { get; set; }
       
    }
    public class IssuePayload
    {
        public Fields fields { get; set; }
    }

    public class Fields
    {
        public Project project { get; set; }
        public string summary { get; set; }
        public string description { get; set; }
        public IssueType issuetype { get; set; }
    }

    public class Project
    {
        public string key { get; set; }
    }

    public class IssueType
    {
        public string name { get; set; }
    }
}
