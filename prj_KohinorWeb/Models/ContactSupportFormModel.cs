﻿namespace prj_KohinorWeb.Models
{
    public class ContactSupportFormModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Subject { get; set; }
        public string Company { get; set; }
        public string Description { get; set; }
    }
}
